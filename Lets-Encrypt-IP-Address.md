# 无域名时如何使用Let's Encrypt加密网站

## 场景
在web开发中，调试时，网站通常以IP+端口对外访问，有时候需要对网站https加密才能正常调试，比如在微信中打开或者调用某些功能；调试时，一般只使用免费的HTTPS证书服务，如Let's Encrypt。

会出现的问题包括：

- 没有自己的域名。申请不成功或者无力购买
- 有固定公网IP服务器
- 需要给部署在该服务器的网站，进行免费的HTTPS加密
- Let's Encrypt不支持对IP访问的网站加密，生成SSL证书时，需要域名可访问，才能验证

## Pre Requirement

- 一台固定公网IP的主机
- sudo可执行，80/443端口可访问
- 安装了 nignx
- 安装了 certbot

## xip.io

[![image](https://gitlab.com/jacktan1991/tech-markdown/uploads/80e51e46dc1d2a5edf762ab10a55c260/image.png)](http://xip.io/)

[xip.io](http://xip.io/) 是一个通配DNS映射IP的域名服务。简单说来，就是原来使用IP访问的网站，通过它可以域名访问了，可以很好的解决第一个问题。

以nginx为例，只要在机器上启动了nginx服务，比如通过IP可以成功访问 http://118.89.49.61 ，那么

- http://118.89.49.61.xip.io
- http://www.118.89.49.61.xip.io
- http://api.118.89.49.61.xip.io

都可以访问。原理就是上述URL，DNS解析后都会映射到IP 118.89.49.61。

类似 xip.io 的还有 [xip.name](http://xip.name)

## Let's Encrypt
Let's Encrypt 是一个免费的 CA 机构。网站需要在调试部署时，加上HTTPS证书，使用 Let's Encrypt 是最好的方案。


### certbot的安装和基本使用

通过 [certbot](https://certbot.eff.org) 程序，可以生成ssl文件。

以centos为例，安装步骤如下
```
$ yum -y install yum-utils
$ yum-config-manager --enable rhui-REGION-rhel-server-extras rhui-REGION-rhel-server-optional
$ sudo yum install certbot
```

certbot 用法很多，简单的生成和更新命令如下：

- 交互式生成证书 `certbot certonly`
- 更新证书 `certbot renew`

### 使用 certbot 向 Let's Encrypt 申请证书

交互式生成特定118.89.49.61.xip.io域名的ssl相关文件步骤

```
# 事先关闭 nginx 获取其他占用80端口的服务
sudo certbot certonly
```
然后依次选择或者输入

- 2: Spin up a temporary webserver (standalone)
- 输入完整域名：118.89.49.61.xip.io

上述交互式过程，等效的命令: `sudo certbot certonly --standalone -d 118.89.49.61.xip.io`

生成的证书在 `/etc/letsencrypt/live/118.89.49.61.xip.io` 目录下，按照[官方文档](https://certbot.eff.org/docs/using.html#where-are-my-certificates)的建议，直接使用该目录下的证书文件路径作配置，无需复制或移动目录下的证书文件

## nginx配置

以 118.89.49.61.xip.io 为例，反向代理了本机 8088 的一个web应用


生成证书后的配置

```
upstream xip_default {
    server 127.0.0.1:8088;
}

server {
    listen 80; 
    server_name 118.89.49.61.xip.io;
    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl;
    server_name 118.89.49.61.xip.io;

    ssl_certificate /etc/letsencrypt/live/118.89.49.61.xip.io/fullchain.pem;
    ssl_certificate_key  /etc/letsencrypt/live/118.89.49.61.xip.io/privkey.pem;

    location / { 
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://xip_default;
    }   
}
```

对比生成证书前的 server 配置

```
server {
    listen 80;
    server_name 118.89.49.61.xip.io;

    location / { 
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://xip_default;
    }   
}
```
