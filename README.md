# jupyter-notebook

[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/editable-pen%2Fjupyter-notebook/master)

https://gitlab.com/editable-pen/jupyter-notebook

## 其他在线notebook
- [cocalc](https://cocalc.com/)
- [Colab](https://colab.research.google.com/)

示例项目：https://github.com/jakevdp/PythonDataScienceHandbook

